export class Request {
    constructor(url) {
        this.url = url;
    }
    async get() {
        try {
            const response = await fetch(this.url);
            if (!response.ok) {
                throw new Error(`Request error: ${response.status}`);
            }
            return response.json();
        } catch (error) {
            console.error(error);
        }
    }
}