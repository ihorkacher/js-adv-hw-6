import { URL, fieldsURL } from "./const.js";
import { Request } from "./Request.js";
import { createIpInfoWindow } from "./functions.js"

export class IP {
    async getIpAddress() {
        return (await new Request(URL).get());
    }
    async getIpInformation() {
        const { ip } = await this.getIpAddress();
        return (await new Request(`http://ip-api.com/json/${ip}?${fieldsURL}`).get())
    }
    async renderIpInformation() {
        const { city, continent, country, district, regionName, } = await this.getIpInformation();
        return createIpInfoWindow(city, continent, country, district, regionName);
    }
}