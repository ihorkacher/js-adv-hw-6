export { searchButton, search, loader, URL, fieldsURL };
const search = document.querySelector('.search');
const searchButton = document.querySelector('.search__button');
const loader = document.createElement('div');
loader.className = 'lds-dual-ring';
const URL = 'http://api.ipify.org/?format=json';
const fieldsURL = 'fields=status,message,continent,country,regionName,city,district';