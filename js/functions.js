export function createIpInfoWindow(cityInfo, continentInfo, countryInfo, districtInfo, regionNameInfo) {
    const wrapper = document.createElement('div');
    wrapper.className = 'IpInformation';

    const city = document.createElement('div');
    city.className = 'IpInformation__city';
    city.innerHTML = `City: ${cityInfo}`;

    const continent = document.createElement('div');
    continent.className = 'IpInformation__continent';
    continent.innerHTML = `Continent: ${continentInfo}`;

    const country = document.createElement('div');
    country.className = 'IpInformation__country';
    country.innerHTML = `Country: ${countryInfo}`;

    const district = document.createElement('div');
    district.className = 'IpInformation__district';
    district.innerHTML = `District: ${districtInfo}`;

    const regionName = document.createElement('div');
    regionName.className = 'IpInformation__regionName';
    regionName.innerHTML = `Region: ${regionNameInfo}`;

    wrapper.append(continent, country, regionName, city, district);
    return wrapper;
}