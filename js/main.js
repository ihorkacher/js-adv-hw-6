import { searchButton, search, loader, } from "./const.js";
import { IP } from "./IP.js";

const clickHandler = async () => {
    searchButton.after(loader);
    const ipInfo = await new IP().renderIpInformation();
    search.after(ipInfo);
    searchButton.style.background = 'rgba(36, 197, 255, .3)';
    searchButton.style.fontSize = '22px';
    loader.remove();
    searchButton.removeEventListener('click', clickHandler);
};

searchButton.addEventListener('click', clickHandler);
